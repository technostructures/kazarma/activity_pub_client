# ActivityPubClient

An ActivityPub client library. Developed for [Kazarma](https://gitlab.com/technostructures/kazarma/kazarma), a Matrix bridge, it is intended for use with an important number of accounts.

## Installation

The package can be installed by adding `activity_pub_client` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:activity_pub_client, "~> 0.1.1"}
  ]
end
```

## Usage

### Authentication

```elixir
iex> {:needs_authorization, _client, url} = ActivityPubClient.Client.client("https://ap_instance.com", "https://ap_instance.com/users/alice")
# User should browse to the given URL to get an OAuth2 code
iex> ActivityPubClient.Client.set_code("https://ap_instance.com/users/alice", "X13YCODEY31X")
# Then the previous call will return a Client struct with a valid access token
iex> {:ok, client} = ActivityPubClient.Client.client("https://ap_instance.com", "https://ap_instance.com/users/alice")
```

### Streaming

```elixir
iex> {:ok, client} = ActivityPubClient.Client.client("https://ap_instance.com", "https://ap_instance.com/users/alice")
# Start a new SyncClient process
iex> ActivityPubClient.SyncServer.start(client, "public")
# Open the WebSocket
iex> ActivityPubClient.SyncServer.open_ws(client)
# Receive messages
iex> receive do
...>   {:websocket_open} -> nil
...>   %{activitypub_message: message} -> do_something(message)
...> end
# ActivityPubClient.SyncServer.start/3 can also be used, with a callback
iex> ActivityPubClient.SyncServer.start(client, "public", &do_something/1)
```

### Search

```elixir
iex> {:ok, client} = ActivityPubClient.Client.client("https://ap_instance.com", "https://ap_instance.com/users/alice")
iex> {:ok, %{"accounts" => accounts, "hashtags" => hashtags, "statuses" => statuses}} = ActivityPubClient.Search.search(client, "bob")
```

### Statuses

```elixir
iex> {:ok, client} = ActivityPubClient.Client.client("https://ap_instance.com", "https://ap_instance.com/users/alice")
iex> {:ok, status} = ActivityPubClient.Status.post_status(client, "Hello World!")
```

### Chats

```elixir
iex> {:ok, client} = ActivityPubClient.Client.client("https://ap_instance.com", "https://ap_instance.com/users/alice")
iex> {:ok, %{"accounts" => [%{"id" => account_id}]}} = ActivityPubClient.Search.search(client, "bob@ap_instance")
iex> {:ok, %{"id" => chat_id}} = ActivityPubClient.Chat.get_chat(client, account_id)
iex> {:ok, message} = ActivityPubClient.Chat.post_message(client, "dcba987654321", "Hello!")
```

## API documentation

Documentation can be found at [https://hexdocs.pm/activity_pub_client](https://hexdocs.pm/activity_pub_client/api-reference.html).
