defmodule ActivityPubClient.Repo.Migrations.AddAppsTable do
  use Ecto.Migration

  def change do
    create table("ap_client_apps") do
      add :site, :string
      add :client_id, :string
      add :client_secret, :string

      timestamps()
    end

    create unique_index("ap_client_apps", [:site])
  end
end
