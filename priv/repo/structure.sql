--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: ap_client_apps; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ap_client_apps (
    id bigint NOT NULL,
    site character varying(255),
    client_id character varying(255),
    client_secret character varying(255),
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: ap_client_apps_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.ap_client_apps_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ap_client_apps_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.ap_client_apps_id_seq OWNED BY public.ap_client_apps.id;


--
-- Name: ap_client_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ap_client_users (
    id bigint NOT NULL,
    ap_id character varying(255),
    app_id bigint,
    code character varying(255),
    token jsonb,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


--
-- Name: ap_client_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.ap_client_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ap_client_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.ap_client_users_id_seq OWNED BY public.ap_client_users.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    inserted_at timestamp(0) without time zone
);


--
-- Name: ap_client_apps id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ap_client_apps ALTER COLUMN id SET DEFAULT nextval('public.ap_client_apps_id_seq'::regclass);


--
-- Name: ap_client_users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ap_client_users ALTER COLUMN id SET DEFAULT nextval('public.ap_client_users_id_seq'::regclass);


--
-- Name: ap_client_apps ap_client_apps_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ap_client_apps
    ADD CONSTRAINT ap_client_apps_pkey PRIMARY KEY (id);


--
-- Name: ap_client_users ap_client_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ap_client_users
    ADD CONSTRAINT ap_client_users_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: ap_client_apps_site_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX ap_client_apps_site_index ON public.ap_client_apps USING btree (site);


--
-- Name: ap_client_users_ap_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX ap_client_users_ap_id_index ON public.ap_client_users USING btree (ap_id);


--
-- Name: ap_client_users ap_client_users_app_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ap_client_users
    ADD CONSTRAINT ap_client_users_app_id_fkey FOREIGN KEY (app_id) REFERENCES public.ap_client_apps(id);


--
-- PostgreSQL database dump complete
--

INSERT INTO public."schema_migrations" (version) VALUES (20211115152149);
INSERT INTO public."schema_migrations" (version) VALUES (20211115152158);
