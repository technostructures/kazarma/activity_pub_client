defmodule ActivityPubClient.StatusTest do
  use ExUnit.Case

  import ActivityPubClient.Status
  import Mock

  describe "post_status/3" do
    test "sends the corresponding request" do
      with_mock OAuth2.Client,
        post: fn :client,
                 "/api/v1/statuses",
                 %{status: "Hello World!"},
                 [{"Content-Type", "application/json"}] ->
          {:ok, %OAuth2.Response{body: :status}}
        end do
        assert {:ok, :status} = post_status(:client, "Hello World!")
      end
    end
  end
end
