defmodule ActivityPubClient.User do
  @moduledoc """
  Ecto schema to keep track of connected users.
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "ap_client_users" do
    field(:ap_id, :string)
    field(:code, :string)
    # field(:token, :map)
    embeds_one(:token, Token, on_replace: :update) do
      field(:access_token, :string)
      field(:refresh_token, :string)
      field(:expires_at, :integer)
    end

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:ap_id, :code])
    |> cast_embed(:token, with: &token_changeset/2)
    |> validate_required([:ap_id])
    |> unique_constraint(:ap_id)
  end

  @doc false
  def token_changeset(user, attrs) do
    user
    |> cast(attrs, [:access_token, :refresh_token, :expires_at])
    |> validate_required([:access_token])
  end
end
