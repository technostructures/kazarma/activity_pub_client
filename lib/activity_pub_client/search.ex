defmodule ActivityPubClient.Search do
  @moduledoc """
  Requests to the search API in Mastodon, also implemented in Pleroma.

  Reference: https://docs.joinmastodon.org/methods/search/
  """

  @doc """
  Searches for accounts, hashtags or statuses.
  Implemented by Mastodon and Pleroma.

  Expects a `%ActivityPubClient.Client{}` struct, the query and a
  keyword list of optional options. Options can be:
  - account_id: If provided, statuses returned will be authored only by this account
  - max_id: Return results older than this id
  - min_id: Return results immediately newer than this id
  - type: Enum(accounts, hashtags, statuses)
  - exclude_unreviewed: Filter out unreviewed tags? Defaults to false. Use true when trying to find trending tags
  - resolve: Attempt WebFinger lookup. Defaults to false
  - limit: Maximum number of results to load, per type. Defaults to 20. Max 40
  - offset: Offset in search results. Used for pagination. Defaults to 0
  - following: Only include accounts that the user is following. Defaults to false
  """
  def search(client, query, opts \\ []) do
    url =
      "/api/v2/search"
      |> URI.parse()
      |> Map.put(:query, URI.encode_query(Keyword.put(opts, :q, query)))
      |> URI.to_string()

    case OAuth2.Client.get(client, url) do
      {:ok, %OAuth2.Response{body: resp}} ->
        {:ok, resp}

      {:error, %OAuth2.Response{status_code: 401, body: _body}} ->
        {:error, :unauthorized}

      {:error, %OAuth2.Error{reason: reason}} ->
        {:error, inspect(reason)}
    end
  end
end
